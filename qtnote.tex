%-------------------------------------------------------------------------------
% This file provides a skeleton ATLAS note.
\pdfinclusioncopyfonts=1
% This command may be needed in order to get \ell in PDF plots to appear. Found in
% https://tex.stackexchange.com/questions/322010/pdflatex-glyph-undefined-symbols-disappear-from-included-pdf
%-------------------------------------------------------------------------------
% Specify where ATLAS LaTeX style files can be found.
\RequirePackage{latex/atlaslatexpath}
% Comment out the above line if the files are in a central location, e.g. $HOME/texmf.
%-------------------------------------------------------------------------------
\documentclass[NOTE, atlasdraft=true, texlive=2020, UKenglish]{atlasdoc}
% The language of the document must be set: usually UKenglish or USenglish.
% british and american also work!
% Commonly used options:
%  atlasdraft=true|false This document is an ATLAS draft.
%  texlive=YYYY          Specify TeX Live version (2020 is default).
%  coverpage             Create ATLAS draft cover page for collaboration circulation.
%                        See atlas-draft-cover.tex for a list of variables that should be defined.
%  cernpreprint          Create front page for a CERN preprint.
%                        See atlas-preprint-cover.tex for a list of variables that should be defined.
%  NOTE                  The document is an ATLAS note (draft).
%  PAPER                 The document is an ATLAS paper (draft).
%  CONF                  The document is a CONF note (draft).
%  PUB                   The document is a PUB note (draft).
%  BOOK                  The document is of book form, like an LOI or TDR (draft).
%  txfonts=true|false    Use txfonts rather than the default newtx.
%  paper=a4|letter       Set paper size to A4 (default) or letter.

%-------------------------------------------------------------------------------
% Extra packages:
\usepackage{atlaspackage}
% Commonly used options:
%  biblatex=true|false   Use biblatex (default) or bibtex for the bibliography.
%  backend=bibtex        Use the bibtex backend rather than biber.
%  subfigure|subfig|subcaption  to use one of these packages for figures in figures.
%  minimal               Minimal set of packages.
%  default               Standard set of packages.
%  full                  Full set of packages.
%-------------------------------------------------------------------------------
% Style file with biblatex options for ATLAS documents.
\usepackage{atlasbiblatex}
% Commonly used options:
%  backref=true|false    Turn on or off back references in the bibliography.

% Package for creating list of authors and contributors to the analysis.
\usepackage{atlascontribute}

% Useful macros
\usepackage{atlasphysics}
% See doc/atlas_physics.pdf for a list of the defined symbols.
% Default options are:
%   true:  journal, misc, particle, unit, xref
%   false: BSM, hepparticle, hepprocess, hion, jetetmiss, math, process,
%          other, snippets, texmf
% See the package for details on the options.

% Macro to add to-do notes (for several authors). Uses the todonotes package.
% \ATLnote{JS}{Jane}{green!20}{green!50!black!60}
% add macros \JSnote and \JSinote for notes in the margin and inline.
% Set output=false in order not to print out the notes.
% Comment this out for the final version of the note.
\usepackage[output=true]{atlastodo}

% Files with references for use with biblatex.
% Note that biber gives an error if it finds empty bib files.
\addbibresource{qtnote.bib}
\addbibresource{bib/ATLAS.bib}
\addbibresource{bib/CMS.bib}
\addbibresource{bib/ConfNotes.bib}
\addbibresource{bib/PubNotes.bib}

% Paths for figures - do not forget the / at the end of the directory name.
\graphicspath{{logos/}{figures/}}

% Add your own definitions here (file qtnote-defs.sty).
\usepackage{qtnote-defs}

%-------------------------------------------------------------------------------
% Generic document information.
%-------------------------------------------------------------------------------

% Title, abstract and document.
\input{qtnote-metadata}
% Author and title for the PDF file.
\hypersetup{pdftitle={ATLAS document},pdfauthor={The ATLAS Collaboration}}

%-------------------------------------------------------------------------------
% Content
%-------------------------------------------------------------------------------
\begin{document}

\maketitle

\tableofcontents

% List of contributors - print here or after the Bibliography.
% \PrintAtlasContribute{0.30}
% \clearpage

% List of to-do notes.
% \listoftodos

%-------------------------------------------------------------------------------
\section{Introduction}
\label{sec:intro}
%-------------------------------------------------------------------------------

\subsection{High Granularity Timing Detector}
The large increase of pile-up interactions is one of the main experimental challenges for the HL-LHC physics program. Therefore, a High Granularity Timing Detector (HGTD)\cite{ATLAS-TDR-31}, based on low gain avalanche detector (LGAD), has been proposed for the ATLAS Phase-II Upgrade. Taking into account the typical number of hits per track in the detector, the target average time resolution per track for a minimum-ionising particale is 30 ps at the start of lifelime, increasing to 50 ps at the end of HL-LHC operation. The active detector element is made of LGADs read-out by dedicated front-end electronics ASICs (ALTIROC). In the design, a bare module is made of two parts: an LGAD sensor and two ASICs. The sensor and the ASICs are connected through a flip-chip bump bonding process called hybridization. The ASICs are then connected to the peripheral electronics through the module flex cable. The size of the bare modules is around $2\times4\,cm^2$ with 450 pads, and the nominal numbers of bare modules is 7984, all of them are identical. The size of the pad is $1.3\times 1.3\,mm^2$ and each of the ASIC reads a matrix of 225 pads. The size of the ASIC is about $19\times 22\,mm^2$. 

\subsection{Front End ASIC}
The technology to be used is CMOS TSMC 130 nm. The architecture of the ASIC is similar to the ASICs developed for pixel detectors but with a significantly reduced number of channels, as shown in figure\,\ref{ALTIROC_archi}. The ASIC will transform the analog signal from sensor to the digital information of hit, Time Of Arrival (TOA), Time Over Threshold (TOT). The sensor signal is amplified using a voltage preamplifier with about 1 GHz bandwidth. Currently, there are two alternative kind of preamplifier: Transimpedance preamplifier (TZ) and Voltage preamplifier (VPA). The preamplifier is followed by a fast discriminator. There are two Time to Digital Converter (TDC), one for TOA and one for TOT measurement. For TOA measurement, architecture of TOA TDC is vernier delay line, the leading edge of the discriminator's output works as the start pulse and initializes the TDC operation. The stop is given by the clock. This structure will minimize the power dissipation when hits are absent. The TOA measurements are restricted to a 2.5ns windows centered on the bunch crossing and the quantisation step is 20ps. The falling edge of discriminator ouput provides the stop of the TOT TDC, which also use the leading edge as start, with 40ps quantisation step. The TOT information is used offline to correct the TOA for timewalk effect. After correction, the residual variations are well within 10ps.

\begin{figure}[htbp]
    \centering
    \includegraphics[width=0.65\linewidth]{ALTIROC_archi.png}
    \caption{Global architecture of the ALTIROC ASIC.}
    \label{ALTIROC_archi}
\end{figure}

In order to test and qualify the production and connection of the sensors and ASICs, some prototypes are made. Currently, a $5\times 5$ channels sensor and ALTIROC1 is made and under test. ALTIROC1 is the second ALTIROC ASIC prototype with 25 channels to readout $5\times 5$ sensor cells of $1.3\,mm\, \times\, 1.3\,mm$ with phase shifter. Figure\,\ref{module} shows a typical mini-module which is wire bonded to the test board and high voltage.

\begin{figure}[htbp]
    \centering
    \includegraphics[width=0.5\linewidth]{module.png}
    \caption{A typical mini-module}
    \label{module}
\end{figure}


Up to now, there are three versions of ALTIROC1 chips: ALTIROC1\_V1, ALTIROC1\_V2, ALTIROC1\_V3 and the chips that tested in this task are ALTIROC1\_V2 and ALTIROC1\_V3. For both of them, all pads are accessible, even when the sensor is bump bonded on the ASIC. Figure\,\ref{altiroc_pad} shows the layout of ALTIROC1\_V2. The mandatory pads are for clocks, slow controls (SC), probe signals, command and data transmission. The debug pads are for debug and characterize the ASIC, as well as check the power supplies. In the ALTIROC1\_V2, 15 pads use VPA while the rest 10 pads use TZ preamplifier. For the last pixel of each column, its additional capacitance can be adjusted from 0 to 3.5pF using SC parameters. The additional capacitance on other channels is 0. For ALTIROC1\_V3, all the preamplifier is VPA. Similar to V2, the additional capacitance of all the channels in the first 4 columns can be adjusted from 0 to 7 pF. The pixels located in a same column share the same capacitance set. In the last column, only the capacitance of last pixel can be adjusted.
\begin{figure}[hbpt]
    \centering
    \includegraphics[width=0.6\linewidth]{altiroc_pad.png}
    \caption{layout of ALTIROC1\_V2 ASIC}
    \label{altiroc_pad}
\end{figure}

%-------------------------------------------------------------------------------
\section{Timewalk Correction and High Voltage effect}
\label{sec:timewalk}
The timewalk effect is brought of different rising time of different quantities of charge.\cite{sadrozinski20174dimensional} The large charge will result in a signal that rises faster than the slow one, which means the measured TOA will be earlier than the slow one, as is shown in Figure\,\ref{tw_effect}. In order to correct this effect, TOT information, which corresponds to the size of charge, will be used. As shown in Figure\,\ref{tot_correction}, as long as the formula between the TOT measured value (given different quantities of electric) and the TOA value ($t_1-t_0$ in the figure) is measured, the timewalk effect can be corrected. This method also requires an additional FPGA to apply this formula.

\begin{figure}[htbp]
    \centering
    \includegraphics[width=0.8\linewidth]{timewalk_effect.png}
    \caption{Left side: Signals of different amplitude cross a fix threshold at different times, generating a delay $t_d$ on the on the firing of the discriminator that depends upon the signal amplitude. Right side: a linear signal, with amplitude $S$ and rise time $t_r$ crosses the threshold $V_{th}$ with a delay $t_d$.}
    \label{tw_effect}
\end{figure}

\begin{figure}[htbp]
    \centering
    \includegraphics[width=0.6\linewidth]{tot_correction.png}
    \caption{Illustration of using TOT information to correct the TOA error brought by the timewalk effect. $t_0$ is the actual TOA of the two signal with differernt size, $t_1$ and $t_2$ is the measured TOA respectively. $TOT_1$ and $TOT_2$ is the measured TOT of two signals.}
    \label{tot_correction}
\end{figure}

As a result, it is important to study the formula of TOT and TOA with the amplitude of the signals, in another word, the timing calibration.  A smooth and stable relationship is expected so that the uncertainty of the timewalk correction would be small. Some test have already been done to study this relationship. As the sensor needs a bias voltage to drive the charge, a wire is needed to connect the module with the high voltage (HV). In the TDR, one single HV wire goes through a ring on the PEB board that holds module and connected to the HV source. In the current test PEB board, the HV wires are wire-bonded to a L-shape HV pad. However, these wires seem to make timing calibration unpredictable. Figure\,\ref{TOT-Q_cali} shows preliminary test results presented by OMEGA. The test is about the TOT measurement results of pixels with two types of preamplifier when injecting different values of charge. The left plot is the results when only have ASIC without sensor bonded, there are some small kinks, but the curve is continuous basically. The right plot is the results when ASIC bump-bonded to sensor and bias voltage wire-bonded. The VPA preamplifier results shows that there are discontinuities when the injected charge around 8fC while the channels with TZ preamplifier shows better behavior. The discontinuities will bring difficulties for the calibration of TOT measurement as well as the timewalk correction. It is believed that this circumstance is due to the inductance brought by the bias voltage wire-bonding and the VPA preamplifier is thus been affected. This QT task aims to quantify this impact and optimize the best way of bias voltage connection.

\begin{figure}[htbp]
    \centering
    \includegraphics[width=0.8\linewidth]{TOT-Q_cali.png}
    \caption{Test result of TOT behaviour done by OMEGA. The left plot is results with ASIC alone, the right plot is results with ASIC bump bonded to sensor and bias voltage wire-bonded }
    \label{TOT-Q_cali}
\end{figure}

As it is believed that the discontinuities is brought by the inductance of HV wires, this QT task try to find if the timing calibration have some dependence on the number of HV wires. Few test boards which have good performance are selected (reasonable TOA delay test, TOT width test, threshold results and timewalk results) and initially they have tens of HV wires. The module is wire-bonded to one specific test board, so the ID of the board is used to distinguish different module. The wires are then gradually removed and timewalk test is done with different number of wires for comparison. As it has been confirmed that different preamplifiers have different response to the HV wires, the results are divided into two categories based on the type of preamplifier.

%-------------------------------------------------------------------------------


%-------------------------------------------------------------------------------
\section{Test Procedures}
\label{sec:test}
%-------------------------------------------------------------------------------
\subsection{Test System}
In the past one year, several sensors and ALTIROC1 ASICs are manufactured, assembled and then delivered to IHEP. A test system is built to verify the function of the modules, including the study of timewalk effect. All the tests done in this QT Task are based on this system. Figure\,\ref{test_system_schem} is a schematic diagram of the test system and Figure\,\ref{test_system} is a photo of the test system. The PC is connected to the FPGA board (the black one) by Ethernet to setup the firmware and software of FPGA and receive the output from FPGA. The FPGA board is designed by SLAC, in order to control the ALTIROC1 test board and readout the data. The FPGA then connected to the ALTIROC1 test board through PCI-E connection. The ASIC test board is designed by OMEGA, whose major functionality includes controlling ASIC, power supply and voltage measurement. The ASIC's pad is connected to the pins on test board by wire bonding and the sensor is also connected to HV pads in the same way. The HV is supplied by external voltage source. There is an additional interface board which can be added between FPGA and ASIC boards, which is used to filter digital signals provided by FPGA. It can also translate the TOA busy CLPS signal into CMOS signal, and this function is disabled in this test as it is designed for testbeam only.

\begin{figure}[htbp]
    \centering
    \includegraphics[width=0.65\linewidth]{test_system_illus.png}
    \caption{Schematic diagram of the test system}
    \label{test_system_schem}
\end{figure}

\begin{figure}[htbp]
    \centering
    \includegraphics[width=0.65\linewidth]{test_system.jpg}
    \caption{Photo of the test system}
    \label{test_system}
\end{figure}

All the different kinds of test need a signal that is controllable, so the signals are generated by ASIC to mimic the signals from sensor. The signals are controlled by the $cmd\_pulse$ input which is generated by the FPGA and synchronous to 40 $MHz$. There is also an external trigger that can characterize the TOT TDC or TOA TDC, which is sometimes enabled when doing measurement of signals with different delay and width.

\subsection{Timewalk test}
As have been mentioned above, the timewalk test is for testing the TOA and TOT measured results given different size of signals starting at the same time. It is desired that the TOT v.s. the signal amplitude would be in a good shape, in the other words, would be smooth, continuous and distinguishable. Figure\,\ref{tw_example} shows example timewalk test results. Note that larger TOA readout values indicate earlier time of arrival. This example is measured with a pixel using VPA of one ASIC+sensor module, but no HV wires bonded. In this case, the timewalk effect can be corrected with small error due to the smooth and continuous relationship between TOT and TOA. It is hoped that after HV wire-bonding, the relationship of TOT-Q and TOT-TOA could be similar to these results instead of the shape in Figure\,\ref{TOT-Q_cali}.
\begin{figure}[htbp]
    \centering
    \includegraphics[width=0.9\linewidth]{tw_example.pdf}
    \caption{Example results of timewalk test. From up to down is: TOA readout value v.s. injected charge (top), TOTc readout value v.s. injected charge (middle) and TOA readout value v.s TOTc readout value (bottom).}
    \label{tw_example}
\end{figure}

Besides from the timewalk test, there are some other tests need to be done to ensure the module is working properly, which is listed in the following.

\subsubsection{TOA measurement}
The TOA test is for testing the relationship of 7 bit TOA ouput between the actual arrival time, in another word, the quantisation step. The designed value of the step is 20 ps. The FPGA will send commands to generate signals with programmed delay and receive the TOA output of the ASIC, and estimate the step using the least square estimation. Figure\,\ref{TOA_example} shows typical results of TOA results. The upper left panel shows the plot of TOA readout value v.s. delay value of the signal. One unit of the delay value is 9.5582 ps, which is measured in July 2019. The slope of the non-zero part can be translated to the quantisation step. The other panels show the jitter and distribution of the TOA measurement.
\begin{figure}[htbp]
    \centering
    \includegraphics[width=0.6\linewidth]{TOA_example.png}
    \caption{An example results of TOA test}
    \label{TOA_example}
\end{figure}

\subsubsection{coarse TOT measurement}
Similar to TOA test, coarse TOT (TOTc) test aims to measure the relationship between the 7-bit TOTc output with the width of signal. The FPGA will generate signals with different values of width, also in the unit of 9.5582 ps and get the output of TOTc measurement. An example of test results is shown in Figure\,\ref{TOT_example}. One unit of TOTc readout is designed to correspond to 160 ps width.

\begin{figure}[htbp]
    \centering
    \includegraphics[width=0.6\linewidth]{TOT_example.png}
    \caption{An example results of TOTc test}
    \label{TOT_example}
\end{figure}

\subsubsection{Threshold scan}
The threshold scan will measure the threshold voltage with a given quantity of electric charge and this test reflects the noise of the signals. The program will gradually change the threshold voltage and measure the number of hit. The low threshold voltage that passes all signals and noises will be removed by a saturated TOA measured value. Figure\,\ref{thres_example} shows example test results of threshold scan with an injected quantity of charge equals to 5 fC. The slope of the falling edge of number of hits indicate the voltage noise. The more dramatically of the edge falls, the smaller is the noise.

\begin{figure}[hbtp]
    \centering
    \includegraphics[width=0.6\linewidth]{threshold_example.png}
    \caption{An example results of threshold scan. The blue dot for the number of TOA hit, no matter the value of it. The red dot for the number of TOA hit that TOA readout value smaller then 127, where 127 means the TOA readout is saturated.}
    \label{thres_example}
\end{figure}

\subsection{High Voltage wires removal}
After doing the above-mentioned test and confirming that some channels in the module is working properly, the HV wires that are bonded to the HV pad are gradually removed using tweezers. For Board 32 (B32), when the remaining number of wires is larger than 5 before removal, two wires will be removed, one on the upper side and one on the left side. When there will be less then 5 wires left, 1 wire will be removed each time. The numbers of wires on the left and upper side are controlled to be close to each other. The other boards follow the similar strategy. Figure~\ref{B32_photo} is a photo of B32 that is taken when there is only one line left on the left side of the module. Each time wires are removed, the timewalk test is done to study the relationship of TOT, TOA and injected electric quantities. At some points, the TOA delay test and TOT width test is done again to ensure the module is working as expected throughout the whole test. After all HV wires are removed, the HV can no longer imposed on the module and thus the module works like a bare ASIC.

\begin{figure}[htbp]
    \centering
    \includegraphics[width=0.45\linewidth]{IB06_beforeremoval.png}
    \includegraphics[width=0.45\linewidth]{IB06_afterremoval.png}
    \caption{Photo of IB06 with different number of HV wire. Left: With all 20 initially bonded HV wires. Right: Only two HV wires left.}
    \label{B32_photo}
\end{figure}

%-------------------------------------------------------------------------------
\section{Test Results}
As it has been confirmed that channels with different preamplifiers have different reaction to the HV wires, the test results are divided into two parts, based on the preamplifier used in the pixel. There are in total three boards used in this study: B32, IB03 and IB06. The 'I' prefix stands for the boards manufactured by IHEP. Dedicated tests have been done to ensure that they have the same performance with the original boards. For simplicity, in the following part, the channels using voltage preamplifiers will be named as VPA channels and the ones using TZ preamplifiers will be called TZ channels.
\label{sec:result}
\subsection{Voltage Preamplifier}
Two boards are used for the performance of VPA: B32 and IB03. All the channels in them uses VPA so in the following part, there will not be specific statement of what preamplifier does the channel use.

\subsubsection{Board 32}
Board 32 (B32) is one selected test board with ALTIROC1\_v3 module on it. It initially have 31 HV wires, bonded to the up and left side of the sensor evenly (in the direction same as Figure\,\ref{altiroc_pad}). The TOA using both external trigger and internal trigger, TOT width test and threshold scan shows that this board have a relatively good performance, excepet for a few channels. Some selected test results are shown in Figure\,\ref{b32_toatot}. For the TOA test, most quantisation step is measured to be around 20 $ps$. For TOT test, most quantisation step is measured to be around 160 $ps$. After imposing a bias voltage of 70V, the TOA and TOT results hardly changes. The timewalk test is also done with and without the bias voltage, as is shown in Figure\,\ref{B32_31wires_woHV}, which indicates that the amplitude of the high voltage doesn't matter much.

\begin{figure}[htbp]
    \centering
    \includegraphics[width=0.48\linewidth]{B32_ext_toa.pdf}
    \includegraphics[width=0.48\linewidth]{B32_ext_tot.pdf}
    \caption{B32 TOA test (left) and TOTc test (right) results using external trigger. The numbers in the grid is the measured quantisation step of the corresponding channel, in $ps$. Order of the channels is same as Figure\,\ref{altiroc_pad}. The blank block indicates that channel didn't give reasonable test results.}
    \label{b32_toatot}
\end{figure}

\begin{figure}[htbp]
    \centering
    \includegraphics[width=0.7\linewidth]{B32_31wires_woHV.png}
    \caption{TOTc v.s. injected electric quantities relationship of channel 9 in B32, the green dot is results with 70V bias voltage imposed and the orange one is without bias voltage.}
    \label{B32_31wires_woHV}
\end{figure}

The left plot of Figure\,\ref{B32_TOT-Q_70V} shows the results of TOTc v.s. injected electric quantities (TOTc-Q curve) with different number of wires. 70V high voltage is imposed in the case there are wires connected. For 0 wire, as no voltage can be imposed, the results is marked with triangle. From this plot, it can be found that when there is no wire connected, the performance is quite stable, the TOTc-Q curve is smooth and continuous. As long as there is HV wire connected to the module, there appear a platform, ranging from around 20fC to 40fC, where the TOTc remains almost flat with the value around 60. The right plot of Figure\,\ref{B32_TOT-Q_70V} shows the results of TOA v.s. TOTc with different number of wires. Due to the above-mentioned platform at TOT readout value around 60 in the case there have HV wires connected, the slopes of TOA-TOT curves also change dramatically at this point. In comparison, the 0 wire case gives a smooth curve. It means that using this module, when the signal amplitude ranges from 20fC to 40fC, the timewalk correction have large error as little deviation in TOTc readout value will translate to large deviation of TOA readout value. Though from these plot, it can be found that increasing the number of HV wires can help make these curves smooth and more like the case that there is no HV wires, but the improvement is quite limited.

\begin{figure}[htbp]
    \centering
    \includegraphics[width=0.45\linewidth]{B32_TOT-Q_70V.png}
    \includegraphics[width=0.45\linewidth]{ch9_TOT_TOA.png}
    \caption{B32 channel 9 test result of TOTc v.s. Q (left) and TOA v.s. TOTc (right) with different number of wires, 70V bias voltage imposed (except for 0 wire that have no voltage). Different colors stand for results of different number of wires connected.}
    \label{B32_TOT-Q_70V}
\end{figure}


Same tests are done with wires connected but without high voltage imposed, which are shown in Figure\,\ref{B32_0V}. The results indicate that even without high voltage imposed, the VPA will still be affected and the timewalk correction will have large error. These results mean that the main reason for the discontinuities of VPA is not the electric fields generated by the HV wires. Additionally, in some channels, imposing high voltage will give a more stable TOT readout value at large Q, but not a common case for all channels, so it is considered as a coincidence. 

\begin{figure}[htbp]
    \centering
    \includegraphics[width=0.45\linewidth]{ch9_TOTQ_0V.png}
    \includegraphics[width=0.45\linewidth]{ch9_TOT_TOA_0V.png}
    \caption{B32 channel 9 test result of TOT v.s. Q (left) and TOA v.s. TOT (right) with different number of wires, no bias voltage imposed. Different colors stand for results of different number of wires connected.}
    \label{B32_0V}
\end{figure}

Other channels of B32 shows the similar results. Figure\,\ref{B32_ch10_TOTratio} shows the results of channel 10. The y-axis is the TOTc readout value of different number of wires divided by the TOTc readout value of 0 wire. It also shows that the amplifier is affected mostly when the injected electric charges are around 30fC. 

\begin{figure}[htbp]
    \centering
    \includegraphics[width=0.45\linewidth]{ch10_TOTratio_0V.png}
    \includegraphics[width=0.45\linewidth]{ch10_TOTratio_70V.png}
    \caption{B32 channel 10 test result of TOT divided by TOT value of 0 wires. The left plot is without high voltage imposed and the right one is with 70V. Different colors stand for results of different number of wires connected.}
    \label{B32_ch10_TOTratio}
\end{figure}

Figure\,\ref{B32_ch9_TOTder} shows the derivatives of TOA-TOTc curve. The left plot is B32 channel 10 test result of TOA v.s. TOTc, the measured values are in dot and the solid line is the fitted curve using B-Spline function. The right plot is the derivatives of the fitted curve, which can be considered as the reflection of timewalk correction uncertainty with the assumption that the TOTc measurement have a consistent uncertainty. Figure\,\ref{B32_ch10_TOTder} are the results of channel 10 but without bias voltage imposed on the module. As the plots show, as long as the wires are connected even without bias voltage imposed, the performance of time calibration will suffer. From these plots, it can be concluded that the HV wire itself will bring the time calibration unstable and large uncertainty.
\begin{figure}[h]
    \centering
    \includegraphics[width=0.45\linewidth]{ch9_TOT-TOA_fit.png}
    \includegraphics[width=0.45\linewidth]{ch9_TOT-TOA_deriv.png}
    \caption{Left: B32 channel 9 test result of TOA v.s. TOTc, the measured values are in dot and the solid line is the fitted curve using B-Spline. The left plot the derivatives calculated with the fitted curve. These tests are done with 70V bias voltage imposed (except for 0 wire that have no voltage). Different colors stand for results of different number of wires connected.}
    \label{B32_ch9_TOTder}
\end{figure}

\begin{figure}[h]
    \centering
    \includegraphics[width=0.45\linewidth]{ch10_TOT-TOA_fit.png}
    \includegraphics[width=0.45\linewidth]{ch10_TOT-TOA_deriv.png}
    \caption{Left: B32 channel 10 test result of TOA v.s. TOTc, the measured values are in dot and the solid line is the fitted curve using B-Spline. The left plot the derivatives calculated with the fitted curve. These tests are done without bias voltage imposed. Different colors stand for results of different number of wires connected.}
    \label{B32_ch10_TOTder}
\end{figure}


\subsubsection{IB03}
The IB03 is another selected boards for testing the VPA channels. It aslo have an ALTIROC1\_v3 module on it and initially have 20 HV wires bonded, evenly connected to the HV pad. TOA delay test and TOT width test using internal pulser and external pulser are done to ensure the function of this test board. Unluckily, only the last column of the module is working properly due to the poor bump-bonding. 

Figure\,\ref{IB03_ch22_TOT-Q_70V} shows the test results of channel 22. The left plot is the TOTc-Q curve with 70V high voltage imposed and the right plot is the TOA-TOTc curve. The results are similar to those of B32, there is a flat platform at Q around 20fC and the TOA-TOTc curve have some discontinuities when TOTc readout value is around 40. 
\begin{figure}[htbp]
    \centering
    \includegraphics[width=0.45\linewidth]{ch22_TOTc-Q_70V.png}
    \includegraphics[width=0.45\linewidth]{ch22_TOTc-TOA_70V.png}
    \caption{IB03 channel 22 test result of TOTc v.s. Q (left) and TOA v.s. TOTc (right) with different number of wires, 70V bias voltage imposed (except for 0 wire that have no voltage). Different colors stand for results of different number of wires connected.}
    \label{IB03_ch22_TOT-Q_70V}
\end{figure}

Figure\,\ref{IB03_deriv} shows the results of TOA v.s. TOT with B-Spline fit and the derivatives of the fitted curve, which are similar to the B32 test results. Having HV wires will bring larger fluctuation to the derivatives, which means larger error for timewalk correction.

\begin{figure}[htbp]
    \centering
    \includegraphics[width=0.45\linewidth]{IB03_TOA-TOT.png}
    \includegraphics[width=0.45\linewidth]{IB03_deriv.png}
    \caption{IB03 channel 23 test result of TOA v.s. TOT (left) with B-Spline fit and the derivatives of the fitted curve (right) with different number of wires, 70V bias voltage imposed (except for 0 wire that have no voltage). Different colors stand for results of different number of wires connected.}
    \label{IB03_deriv}
\end{figure}

From these results and the results of B32, it can be concluded that the HV wires will have a significant impact on the performance of channels that uses VPA, regardless the number of the HV wires and whether the bias voltage is imposed. The number of HV wires has some minor effect, the performance of timing calibration will be slightly better when increasing the number of HV wires. However, the improvement is small and there will always be large difference with the case when there is no HV wires connected. The amplitude of high voltage also has a minor effect. Discontinuities in time calibration exists in both 70V bias voltage and 0 bias voltage, as long as the HV wires are connected. It indicates that the effect may not due to the electric field brought by the wires.


\subsection{TZ Preamplifier}

From the previously results, it can be seen that the TZ preamplifier seem to be hardly affected by the HV wires. However, the drawback of using TZ preamplifier is that the TOTc readout is insensitive to the injected electric quantities. When the Q is increased to larger than 20 fC, the TOTc readout will almost remain same.

One test board is selected to test the channels using TZ preamplifier: IB06. This board have ALTIROC1\_v3b on it, which have 15 TZ channels and 10 VPA channels. There are 20 HV wires connected initially. The regular tests show that the VPA channels are not working properly, so only the TZ channels are used for further test. Additionally, the TOA TDC of this module is broken so only the TOTc-Q curve can be used.

The selected timewalk test results are shown in Figure\,\ref{IB06_results}. The left plot is channel 11 test results with 20 HV wires and imposing differnt amplitude of bias voltage, which indicate that the performance of TOT measurement is hardly affected by the amplitude of bias voltage for TZ channels. The right plot is channel 4 test results with different number of HV wires, 70V bias voltage imposed. This plot shows that the number of HV wires have mildly effect on the TOT measurements of TZ channels. In general, the TZ channels work consistently with or without HV wires.

\begin{figure}[htbp]
    \centering
    \includegraphics[width=0.45\linewidth]{IB06_20wires_diffV.png}
    \includegraphics[width=0.45\linewidth]{IB06_diffwire_ch4.png}
    \caption{IB03 test result of TOTc v.s. Q. Left: channel 11 test results with 20 HV wires and imposing differnt amplitude of bias voltage. Different colors stand for different voltage amplitude. Right: channel 4 test results with different number of HV wires, 70V bias voltage imposed. Different colors stand for results of different number of wires connected.}
    \label{IB06_results}
\end{figure}
%-------------------------------------------------------------------------------
% All figures and tables should appear before the summary and conclusion.
% The package placeins provides the macro \FloatBarrier to achieve this.
% \FloatBarrier

%-------------------------------------------------------------------------------
\section{Conclusion}
\label{sec:conclusion}
%-------------------------------------------------------------------------------

Given all the test results shown above in this QT Task, it can be confirmed that the HV wires have different degrees of effect on two kinds of preamplifiers. For channels using VPA, the HV wires will have a significant effect on the performance of timing calibration, which makes the error of timewalk correction large. Moreover, changing number of HV wires would relieve this effect, but only with limited improvement. Having a dense number of HV wires has a slightly better performance of timing calibration then having only one HV wire. As a result, the inductance between the HV wires could be a really minor reason for this effect. Because this effect exists as long as the wire connected, even without bias voltage imposed, the electric field generated by the HV wires may also not be the reason for the effect. Changing the method of high voltage (e.g. using conductive glue) would also hardly help and difficult to conduct in the final module assembly. By contrast, the channels using TZ preamplifier are hardly affected by the HV wires. Thus using TZ preamplifiers seems to be the best solution to this high voltage connection problem. However, TZ preamplifier makes TOT measurement insensitive to the size of injected charge, which will also bring some error to the timewalk correction. Further study can be done with the ALTIROC2 prototype to better understand the impact of HV wires and find some other solutions. 

%-------------------------------------------------------------------------------
% If you use biblatex and either biber or bibtex to process the bibliography
% just say \printbibliography here.
\printbibliography
% If you want to use the traditional BibTeX you need to use the syntax below.
% \bibliographystyle{obsolete/bst/atlasBibStyleWithTitle}
% \bibliography{qtnote,bib/ATLAS,bib/CMS,bib/ConfNotes,bib/PubNotes}
%-------------------------------------------------------------------------------

%-------------------------------------------------------------------------------
% Print the list of contributors to the analysis.
% The argument gives the fraction of the text width used for the names.
%-------------------------------------------------------------------------------

%-------------------------------------------------------------------------------
\clearpage
\appendix
\part*{Appendices}
\addcontentsline{toc}{part}{Appendices}
%-------------------------------------------------------------------------------

All the test results are available in the cernbox: https://cernbox.cern.ch/index.php/s/gP9JKza7TxMYqmG

All the code used in this QT Task can be found in gitlab: https://gitlab.cern.ch/yuhao/hgtd\_module\_test\_analysis

The Latex source code can be found in: https://gitlab.cern.ch/yuhao/qtnote


\end{document}
